from django.contrib import admin

# Register your models here.
from .models import TaskData
admin.site.register(TaskData)