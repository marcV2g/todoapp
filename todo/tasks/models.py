import datetime
from django.db import models
from django.utils import timezone


class TaskData(models.Model):
    name = models.CharField(max_length=32)
    desc = models.CharField(max_length=200)
    state = models.CharField(max_length=16)
    createDate = models.DateTimeField('date created')
    dueDate = models.DateTimeField('date due')

    def __str__(self):
        return self.name