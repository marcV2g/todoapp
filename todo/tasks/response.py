from django.http import HttpResponse
import json

class JsonHttpResponse(HttpResponse):
    def __init__(self, obj):
        resp = {
            "status": "ok",
            "data": obj,
        }
        str = json.dumps(resp, default=lambda x: x.__dict__)
        super().__init__(str)

class StatusHttpResponse(HttpResponse):
    def __init__(self,status,msg ):
        obj = {
            "status" : status,
            "debug" : msg,
        }
        str = json.dumps(obj, default=lambda x: x.__dict__)
        super().__init__(str)

class ErrorHttpResponse(StatusHttpResponse):
    def __init__(self, errorMsg):
        super().__init__("error", errorMsg)