from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('addTask', views.addTask, name='addTask'),
    path('listTask', views.listTask, name='listTask'),
    path('deleteTask', views.deleteTask, name='deleteTask'),
    path('updateTask', views.updateTask, name='updateTask'),
]
