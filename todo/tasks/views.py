from django.shortcuts import render
from django.http import HttpResponse
from tasks.models import TaskData
import datetime
import json
from tasks.data import *
from tasks.response import *
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

def index(request):
    return HttpResponse("this is the index")

def addTask(request):
    reqStr = request.body.decode('utf-8')
    if not reqStr:
        return ErrorHttpResponse("no data")
    #reqStr = '{"name": "fail", "desc": "dump task"}'

    obj = json.loads(reqStr)
    print(obj)
    task = TaskData(
        name = obj["name"],
        desc = obj["desc"],
        state = "ready",
        createDate = timezone.now(),
        dueDate = timezone.now(),
    )
    task.save()

    response = {
       "id" :  task.id,
        "name" : task.name,
    }
    return JsonHttpResponse(response)

def deleteTask(request):
    reqStr = request.body.decode('utf-8')
    if not reqStr:
        return ErrorHttpResponse("no data")
    #reqStr = '{"id": 11}'
    req = json.loads(reqStr)
    taskId = req["id"]

    try:
        task = TaskData.objects.get(id = taskId)
    except ObjectDoesNotExist:
        return StatusHttpResponse("fail", "no id: " + str(taskId))

    d = task.delete()
    return StatusHttpResponse("ok",d)


def updateTask(request):
    reqStr = request.body.decode('utf-8')
    #reqStr = '{"id": 10, "state": "ready"}'
    if not reqStr:
        return ErrorHttpResponse("no data")
    req = json.loads(reqStr)
    taskId = req["id"]
    try:
        task = TaskData.objects.get(id = taskId)
        task.state = req["state"]
        task.save()
        return StatusHttpResponse("ok","state updated")
    except ObjectDoesNotExist:
        return StatusHttpResponse("fail", "no id: " + str(taskId))



def listTask(request):
    reqStr = request.body.decode('utf-8')
    #reqStr = '{"state": "ready"}'
    if reqStr:
        req = json.loads(reqStr)
        tasks = TaskData.objects.filter(state = req["state"])
    else:
        tasks = TaskData.objects.all()

    viewList = []
    for task in tasks:
        viewList.append(TaskViewData(task))

    return JsonHttpResponse(viewList)