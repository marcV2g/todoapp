import requests
import json
from tasks.data import *

#'http://127.0.0.1:8000/tasks/listTask'

def call(method,reqObj=None):
    url = 'http://127.0.0.1:8000/tasks/' + method
    if reqObj:
        reqStr = json.dumps(reqObj)
        print(reqStr)
        r = requests.get(url, data=reqStr)
    else:
        r = requests.get(url)

    print(r.text)
    return json.loads(r.text)


print("begin")

print("list all")
call("listTask")

print("list state = ready")
listRequest = {
    "state": "ready"
}
call("listTask",listRequest)

print("add element")

addRequest = {
"name": "dummy",
"desc": "dummy task",
}

addResp = call("addTask", addRequest)
id = addResp["data"]["id"]
print("id: " + str(id))

print("update element state")
updateRequest = {
    "id" : id,
    "state": "done",
}
call("updateTask", updateRequest)

print("delete element")

deleteRequest = {
    "id" : id,
}
call("deleteTask", updateRequest)

print("end")